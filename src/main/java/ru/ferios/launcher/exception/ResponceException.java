package ru.ferios.launcher.exception;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

public class ResponceException extends LauncherException {

    @SerializedName("error")
    @Getter
    private String message;

}
