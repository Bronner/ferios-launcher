package ru.ferios.launcher;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public final class ThreadManager {

    private static final ExecutorService executorService = Executors.newCachedThreadPool();

    public static void execute(Runnable runnable) {
        executorService.submit(runnable);
    }

    public static void shutdown() {
        executorService.shutdown();
    }
}
