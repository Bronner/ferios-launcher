package ru.ferios.launcher;

public final class Properties {

    public static final String FILE_NAME = "ferioslauncher";

    public static final boolean DEBUG = true;

    public static final String AUTH_ADDR = "localhost";

    public static final int AUTH_PORT = 25500;

    public static final String API_AUTH = "http://api.ferios.ru/v1/launcher/auth";

    public static final String API_LAUNCHER = "http://api.ferios.ru/v1/launcher";

    public static final String API_SERVERS = "http://api.ferios.ru/v1/launcher/servers";

    public static final String API_CLIENT = "http://api.ferios.ru/v1/launcher/clients";

    public static final String API_LIBS = "http://api.ferios.ru/v1/launcher/libraries";

    public static final String API_MODS = "http://api.ferios.ru/v1/launcher/mods";

    public static final String[] ASSETS_REPO = {"http://resources.download.minecraft.net/"};

    public static final String[] LIBRARIES_REPO = {"http://res.ferios.ru/game/libraries/",
            "https://libraries.minecraft.net/", "http://s1.mmods.ru/launcher/libraries/"};

    public static final String[] MODS_REPO = {"http://res.ferios.ru/game/client_{name}/mods/"};

    public static final String[] COMMONS_REPO = {"http://res.ferios.ru/game/commons/"};

    public static final String FOLDER_NAME = "Ferios-beta";

    public static final String LIB_DIR = "lib";

    public static final String CLIENT_DIR = "bin";

    public static final int MODS_CHECK_TIME = 30000;

    public static final int MODS_CHECK_COUNT = 3;

    public static final int[] MEMORY = {512, 768, 1024, 2048, 3072, 4096};

    private Properties() {
    }
}
