package ru.ferios.launcher;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import ru.ferios.launcher.config.Settings;
import ru.ferios.launcher.domain.Credentials;
import ru.ferios.launcher.domain.LaunchCommandBuilder;
import ru.ferios.launcher.domain.ResourceObject;
import ru.ferios.launcher.domain.Server;
import ru.ferios.launcher.util.MinecraftUtil;
import ru.ferios.launcher.util.java.eURLClassLoader;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.*;

import static com.google.common.base.Preconditions.checkNotNull;

@Slf4j
class GameLoader {

    private static boolean valid = true;

    private Launcher launcher;

    void start(final Credentials credentials, final Launcher launcher) {
        checkNotNull(credentials);
        checkNotNull(launcher);

        this.launcher = launcher;
        //todo launcher.onGameStarting();
        Server server = credentials.getServer();
        log.info("Запуск Minecraft");

        try {
            File workDir = MinecraftUtil.getWorkingDirectory();
            final File clientDir = new File(workDir, Properties.CLIENT_DIR);
            final File[] libraries = getListFiles(MinecraftUtil.getFile(Properties.LIB_DIR), server.getLibList());
            final File modsDir = MinecraftUtil.getFile("mods");
            File clientJar = new File(clientDir, server.getClientName() + ".jar");

            if (!modsDir.exists() || !modsDir.isDirectory()) {
                Files.createDirectories(modsDir.toPath());
            }
            File serverModsDir = MinecraftUtil.getFile("mods_" + server.getModListName());
            if (FileUtils.sizeOfDirectory(modsDir) != FileUtils.sizeOfDirectory(serverModsDir)) {
                //todo per file check
                FileUtils.cleanDirectory(modsDir);
                FileUtils.copyDirectory(serverModsDir, modsDir);
            }

            final long originalModsSize = FileUtils.sizeOfDirectory(serverModsDir);
            if (FileUtils.sizeOfDirectory(modsDir) != originalModsSize
                    || clientJar.length() != server.getClient().getSize()) {
                //todo launcher.onGameStartError();
                return;
            }

            new Thread() {
                Timer timer;

                public void run() {
                    timer = new Timer(Properties.MODS_CHECK_TIME, new ActionListener() {
                        int checkCount = 0;

                        public void actionPerformed(ActionEvent e) {
                            if (FileUtils.sizeOfDirectory(modsDir) != originalModsSize) {
                                System.err.println("Game failed to start, try again?");
                                valid = false;
                                //                                    Runtime.getRuntime().exit(0);
                            }
                            if (++checkCount > Properties.MODS_CHECK_COUNT) {
                                timer.stop();
                            }
                        }
                    });
                    timer.start();
                }
            }.start();

            final File natives = new File(clientDir, "natives");
            List<URL> listUrls = getClassPath(libraries);
            listUrls.add(clientJar.toURI().toURL());

            System.setProperty("fml.ignoreInvalidMinecraftCertificates", "true");
            System.setProperty("fml.ignorePatchDiscrepancies", "true");
            System.setProperty("org.lwjgl.librarypath", natives.getAbsolutePath());
            System.setProperty("net.java.games.input.librarypath", natives.getAbsolutePath());
            System.setProperty("java.library.path", natives.getAbsolutePath());

            LaunchCommandBuilder params = new LaunchCommandBuilder();
            params.username(credentials.getLogin());
            params.version(server.getClientVersion());
            params.gameDir(workDir.getAbsolutePath());
            params.assetsDir(workDir.getAbsolutePath() + File.separator + "assets");

            final eURLClassLoader cl = new eURLClassLoader(listUrls.toArray(new URL[listUrls.size()]));
            try {
                cl.loadClass("com.mojang.authlib.Agent");
                params.assetIndex(server.getAssets());
                params.uuid(credentials.getUuid());
                params.accessToken(credentials.getSession());
            } catch (ClassNotFoundException e) {
                // Error
            }

            String[] tweaks = new String[]{
                    "com.mumfrey.liteloader.launch.LiteLoaderTweaker",
                    "cpw.mods.fml.common.launcher.FMLTweaker",
                    "optifine.OptiFineForgeTweaker"
            };

            for (String tweak : tweaks) {
                try {
                    cl.loadClass(tweak);
                    params.tweakClass(tweak);
                } catch (ClassNotFoundException ignored) {
                    //NOP
                }
            }

            if (Settings.getInstance().isAutologin()) {
                params.server(server.getIp(), String.valueOf(server.getPort()));
            }

            if (Settings.getInstance().isFullscreen()) {
                params.fullscreen();
            } else {
                params.resolution(String.valueOf(Settings.getInstance().getSizes()[0]),
                        String.valueOf(Settings.getInstance().getSizes()[1]));
            }

            String[] args = params.build();
            String aClass;
            if (params.isTweak()) {
                aClass = "net.minecraft.launchwrapper.Launch";
            } else {
                aClass = "net.minecraft.client.main.Main";
            }

            Class<?> start = cl.loadClass(aClass);
            Class<?>[] argTypes = new Class<?>[]{String[].class};
            final Method main = start.getDeclaredMethod("main", argTypes);

            Thread t1 = new Thread("Minecraft Window") {
                @Override
                public void run() {
                    try {
                        main.invoke(null, (Object) args); //todo проверить нужен ли здесь каст
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        log.error("Er", e);
                        JOptionPane.showMessageDialog(null, e, "Ошибка при запуске", JOptionPane.ERROR_MESSAGE, null);
                        Runtime.getRuntime().exit(0);
                    }
                }
            };
            t1.setDaemon(true);
            t1.start();
            String t1Name = t1.getName();
            while (t1Name.equals(t1.getName())) {
                Thread.sleep(500);
            }

            //            launcher.onGameStarted();

            while (valid) {
                Thread.sleep(Properties.MODS_CHECK_TIME);
            }

            Class<?> af = cl.loadClass("java.lang.Shutdown");
            Method m = af.getDeclaredMethod("halt0", int.class);
            m.setAccessible(true);
            m.invoke(null, 0);
        } catch (Exception e) {
            log.error("Er", e);
            JOptionPane.showMessageDialog(null, e, "Ошибка при запуске", JOptionPane.ERROR_MESSAGE, null);
            Runtime.getRuntime().exit(0);
        }
    }

    private File[] getListFiles(File dir, Collection<ResourceObject> libList) {
        File[] files = new File[libList.size()];
        int i = 0;
        for (ResourceObject ro : libList) {
            files[i++] = new File(dir, ro.getPath());
        }
        return files;
    }

    private List<URL> getClassPath(File[] files) throws IOException {
        List<URL> urls = new ArrayList<>();
        for (File file : files) {
            if (file.isFile()) {
                urls.add(file.toURI().toURL());
            }
        }
        return urls;
    }

}
