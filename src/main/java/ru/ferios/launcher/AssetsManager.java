package ru.ferios.launcher;

import com.google.gson.JsonSyntaxException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import ru.ferios.commons.downloader.DownloadUrl;
import ru.ferios.commons.downloader.downloads.Downloadable;
import ru.ferios.launcher.domain.Server;
import ru.ferios.launcher.domain.AssetIndex;
import ru.ferios.launcher.domain.AssetIndex.AssetObject;
import ru.ferios.launcher.util.FileUtil;
import ru.ferios.launcher.util.MinecraftUtil;
import ru.ferios.launcher.util.json.JsonHelper;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
public class AssetsManager {

    private final Object assetsFlushLock;

    public AssetsManager() {
        this.assetsFlushLock = new Object();
    }

    public Set<Downloadable> getResourceFiles(File baseDirectory, List<AssetObject> list) {
        Set<Downloadable> result = new HashSet<Downloadable>();

        File objectsFolder = new File(baseDirectory, "assets/objects");
        for (AssetObject object : list) {
            String filename = object.getFilename();
            DownloadUrl url = new DownloadUrl(filename, Properties.ASSETS_REPO);
            Downloadable d = new Downloadable(new File(objectsFolder, filename), url);
            d.setExpectedSize(object.getSize());
            result.add(d);
        }
        return result;
    }

    public List<AssetObject> checkResources(Server server, boolean fast) {
        return checkResources(server, MinecraftUtil.getWorkingDirectory(), false, fast);
    }

    List<AssetObject> checkResources(Server server, File baseDirectory, boolean local,
            boolean fast) {
        log.info("Checking resources...");

        List<AssetObject> list;
        List<AssetObject> r = new ArrayList<AssetObject>();

        if (local) {
            list = getLocalResourceFilesList(server, baseDirectory);
        } else {
            list = getResourceFiles(server, baseDirectory, true);
        }

        if (list == null) {
            log.info("Cannot get assets list. Aborting.");
            return r;
        }

        log.info("Fast comparing: " + fast);

        for (AssetObject resource : list) {
            if (!checkResource(baseDirectory, resource, fast)) {
                r.add(resource);
            }
        }

        return r;
    }

    private List<AssetObject> getLocalResourceFilesList(Server server, File baseDirectory) {
        List<AssetObject> result = new ArrayList<AssetObject>();

        String indexName = server.getAssets();

        File indexesFolder = new File(baseDirectory, "assets/indexes/");
        File indexFile = new File(indexesFolder, indexName + ".json");

        log.info("Reading indexes from file" + indexFile);

        String json;
        try {
            json = FileUtils.readFileToString(indexFile);
        } catch (Exception e) {
            log.info("Cannot read local resource files list for index: " + indexName, e);
            return null;
        }

        AssetIndex index = null;

        try {
            index = JsonHelper.fromJson(json, AssetIndex.class);
        } catch (JsonSyntaxException e) {
            log.info("JSON file is invalid", e);
        }

        if (index == null) {
            log.info("Cannot read data from JSON file.");
            return null;
        }

        for (AssetObject object : index.getUniqueObjects()) {
            result.add(object);
        }

        return result;
    }

    List<AssetObject> getResourceFiles(Server version, File baseDirectory, boolean local) {
        List<AssetObject> list = null;

        if (!local) {
            try {
                list = getRemoteResourceFilesList(version, baseDirectory, true);
            } catch (Exception e) {
                log.info("Cannot get remote assets list. Trying to use the local one.", e);
            }
        }

        if (list == null) {
            list = getLocalResourceFilesList(version, baseDirectory);
        }

        if (list == null) {
            try {
                list = getRemoteResourceFilesList(version, baseDirectory, true);
            } catch (Exception e) {
                log.info("Gave up trying to get assets list.", e);
            }
        }

        return list;
    }

    private boolean checkResource(File baseDirectory, AssetObject local, boolean fast) {
        String path = local.getFilename();

        File file = new File(baseDirectory, "assets/objects/" + path);
        long size = file.length();

        if (!file.isFile() || size == 0L) {
            return false;
        }

        if (fast) {
            return true;
        }

        // Checking size
        if (local.getSize() != size) {
            return false;
        }

        // Checking hash
        if (local.getHash() == null) {
            return true;
        }

        return local.getHash().equals(FileUtil.getChecksum(file, "SHA-1"));
    }

    private List<AssetObject> getRemoteResourceFilesList(Server server, File baseDirectory,
            boolean save) throws IOException {
        List<AssetObject> result = new ArrayList<AssetObject>();

        String indexName = server.getAssets();
        if (indexName == null) {
            indexName = "legacy";
        }

        File assets = new File(baseDirectory, "assets");
        File indexesFolder = new File(assets, "indexes");

        File indexFile = new File(indexesFolder, indexName + ".json");

        log.info("Reading from repository...");

        URL jsonUrl =
                new URL("https://s3.amazonaws.com/Minecraft.Download/indexes/" + indexName + ".json");
        InputStream inputStream = jsonUrl.openConnection().getInputStream();
        String json = IOUtils.toString(inputStream);
        if (save) {
            synchronized (assetsFlushLock) {
                FileUtils.writeStringToFile(indexFile, json);
            }
        }

        AssetIndex index = JsonHelper.fromJson(json, AssetIndex.class);

        for (AssetObject object : index.getUniqueObjects()) {
            result.add(object);
        }

        return result;
    }
}
