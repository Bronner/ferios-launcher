package ru.ferios.launcher;

import lombok.extern.slf4j.Slf4j;
import ru.ferios.commons.utils.Platform;
import ru.ferios.launcher.config.Settings;
import ru.ferios.launcher.util.MinecraftUtil;

import java.util.ArrayList;
import javax.swing.*;

@Slf4j
public final class Bootstrap {

    private Bootstrap() {
    }

    public static void main(String[] args) {
        try {
            ArrayList<String> params = new ArrayList<>();
            params.add(Platform.getJavaExecutable());
            int userMem = Settings.getInstance().getMemory();

            if (Platform.Arch.x32.isCurrent() && userMem > 1024) {
                userMem = 1024;
            } else if (userMem < 512) {
                userMem = 512;
            }

            params.add("-Xmx" + userMem + "M");
            params.add("-Xms512M");
            //            params.add("-Xincgc");
            params.add("-Xnoclassgc");
            params.add("-XX:+UseParNewGC");
            params.add("-XX:+UseConcMarkSweepGC");
            params.add("-XX:+BindGCTaskThreadsToCPUs");
            params.add("-XX:MaxPermSize=256M");
            params.add("-XX:+AggressiveOpts");
            params.add("-XX:+UseFastAccessorMethods");
            params.add("-Dfile.encoding=UTF-8");
            params.add("-Dsun.java2d.noddraw=true");
            params.add("-Dsun.java2d.d3d=false");
            params.add("-Dsun.java2d.opengl=false");
            params.add("-Dsun.java2d.pmoffscreen=false");
            params.add("-Dforge.forceNoStencil=true");

            if (Platform.WINDOWS.isCurrent()) {
                params.add("-XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump");
            }
            //            params.add("-XX:-OmitStackTraceInFastThrow");

            if (Platform.OSX.isCurrent()) {
                params.add("-Xdock:name=Ferios");
                params.add("-Xdock:icon=" + "favicon.png"); //TODO MacOS Favicon fix
            }

            params.add("-cp");
            params.add(Bootstrap.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
            params.add(Launcher.class.getCanonicalName());
            ProcessBuilder pb = new ProcessBuilder(params);
            pb.directory(MinecraftUtil.getWorkingDirectory());
            pb.start();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e, "Ошибка при запуске", javax.swing.JOptionPane.ERROR_MESSAGE, null);
            log.error("Ошибка при запуске", e);
            System.exit(0);
        }
    }
}
