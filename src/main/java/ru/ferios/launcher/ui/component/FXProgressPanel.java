package ru.ferios.launcher.ui.component;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

public class FXProgressPanel extends VBox {

    private boolean infiniteProgress = false;

    private ProgressIndicator progress;

    private Label statusLabel = new Label();

    private ImageView errorIcon = new ImageView("/images/error.png");

    private BooleanProperty failed = new SimpleBooleanProperty();

    public FXProgressPanel(boolean infiniteProgress) {
        this(64, 64, infiniteProgress);
    }

    public FXProgressPanel(double width, double height, boolean infiniteProgress) {
        this.infiniteProgress = infiniteProgress;
        if (infiniteProgress) {
            progress = new ProgressIndicator();
        } else {
            progress = new ProgressIndicator(0D);
        }
        initListeners();

        setStyle("-fx-background-color: white");
        setOpacity(0.6);
        setSpacing(16);
        setAlignment(Pos.CENTER);

        statusLabel.setWrapText(true);
        statusLabel.setTextAlignment(TextAlignment.CENTER);

        progress.setMaxSize(width, height);
        progress.setPrefWidth(width);
        progress.setPrefHeight(height);

        errorIcon.fitHeightProperty().bind(progress.prefHeightProperty());
        errorIcon.fitWidthProperty().bind(progress.prefWidthProperty());
        errorIcon.setSmooth(true);

        getChildren().addAll(progress, statusLabel);
    }

    private void initListeners() {
        failed.addListener((observable, oldValue, failed) -> {
            if (failed != null) {
                if (failed) {
                    getChildren().clear();
                    getChildren().addAll(errorIcon, statusLabel);
                } else {
                    getChildren().clear();
                    getChildren().addAll(progress, statusLabel);
                }
            }
        });
    }

    public void setStatusText(String statusText) {
        this.statusLabel.setText(statusText);
    }

    public StringProperty getStatusTextProperty() {
        return statusLabel.textProperty();
    }

    public void increaseProgress(double value) {
        progress.setProgress(value);
    }

    public void setFailed(boolean failed) {
        this.failed.setValue(failed);
    }

    public boolean isInfiniteProgress() {
        return infiniteProgress;
    }
}
