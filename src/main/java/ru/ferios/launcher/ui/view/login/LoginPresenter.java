package ru.ferios.launcher.ui.view.login;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import ru.ferios.launcher.domain.Credentials;
import ru.ferios.launcher.domain.Server;
import ru.ferios.launcher.service.AuthorizationProvider;
import ru.ferios.launcher.service.ClientUpdateProvider;
import ru.ferios.launcher.service.ServerListProvider;

import java.net.URL;
import java.util.ResourceBundle;
import javax.inject.Inject;

public class LoginPresenter implements Initializable {

    @FXML
    private TextField loginField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private ComboBox<Server> serverField;

    @FXML
    private Label messageLabel;

    @FXML
    private Button loginButton;

    @Inject
    private ServerListProvider serverListProvider;

    @Inject
    private AuthorizationProvider authorizationProvider;

    @Inject
    private ClientUpdateProvider clientUpdateProvider;

    private ObjectProperty<State> state = new SimpleObjectProperty<>(State.PLAY);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initServerListListeners();
        authorizationProvider.stateProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals(AuthorizationProvider.State.SUCCEED)) {
                clientUpdateProvider.checkUpdates(serverField.getValue());
            } else if (newValue.equals(AuthorizationProvider.State.FAILED)) {
                messageLabel.setText("Какая-то ошибка");
            }
        });
        stateProperty().addListener((observable, oldValue, newState) -> {
            loginButton.setText(newState.title);
            loginButton.setStyle("-fx-background-color: " + newState.color + ";");
        });
        loginField.setText("123");
        passwordField.setText("123");
    }

    void initServerListListeners() {
        serverField.itemsProperty().bind(serverListProvider.valueProperty());
        serverField.valueProperty().addListener((observable, oldValue, newValue) -> {
            setState(State.UPDATE);
        });
    }

    public ObjectProperty<State> stateProperty() {
        return state;
    }

    @FXML
    void handleLoginButton() {
        if (serverField.getValue() == null) {
            showError("Выберите сервер");
            return;
        }
        Credentials credentials =
                new Credentials(loginField.getText(), passwordField.getText(), serverField.getValue());

        if (credentials.isValid()) {
            showError("");
            authorizationProvider.authorize(credentials);
        } else {
            showError("Укажите логин и пароль");
        }
    }

    void showError(String message) {
        messageLabel.setTextFill(Color.RED);
        messageLabel.setText(message);
    }

    @FXML
    void handleSettingsButton() {
        System.out.println(2);
    }

    public State getState() {
        return state.get();
    }

    public void setState(State state) {
        this.state.set(state);
    }

    public enum State {
        INSTALL("Установить", "green"),
        UPDATE("Обновить", "orange"),
        PLAY("Играть", "blue");

        final String title;

        final String color;

        State(String title, String color) {
            this.title = title;
            this.color = color;
        }
    }
}
