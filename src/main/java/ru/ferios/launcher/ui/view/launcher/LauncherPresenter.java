package ru.ferios.launcher.ui.view.launcher;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import lombok.extern.slf4j.Slf4j;
import ru.ferios.launcher.service.AuthorizationProvider;
import ru.ferios.launcher.service.ClientUpdateProvider;
import ru.ferios.launcher.service.ServerListProvider;
import ru.ferios.launcher.ui.component.FXProgressPanel;
import ru.ferios.launcher.ui.view.download.DownloadPresenter;
import ru.ferios.launcher.ui.view.download.DownloadView;
import ru.ferios.launcher.ui.view.login.LoginPresenter;
import ru.ferios.launcher.ui.view.login.LoginView;
import ru.ferios.launcher.ui.view.settings.SettingsPresenter;
import ru.ferios.launcher.ui.view.settings.SettingsView;
import ru.ferios.launcher.updater.LauncherUpdater;

import java.net.URL;
import java.util.ResourceBundle;
import javax.inject.Inject;

@Slf4j
public class LauncherPresenter implements Initializable {

    @FXML
    private StackPane contentHolder;

    @FXML
    private StackPane content;

    @FXML
    private BorderPane progressPane;

    private GaussianBlur blur = new GaussianBlur(15);

    private FXProgressPanel progress;

    private LoginPresenter loginPresenter;

    private SettingsPresenter settingsPresenter;

    private DownloadPresenter downloadPresenter;

    private LoginView loginView;

    private SettingsView settingsView;

    private DownloadView downloadView;

    @Inject
    private LauncherUpdater launcherUpdater;

    @Inject
    private ServerListProvider serverListProvider;

    @Inject
    private AuthorizationProvider authorizationProvider;

    @Inject
    private ClientUpdateProvider clientUpdateProvider;

    public LauncherPresenter() {
        progress = new FXProgressPanel(true);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initUpdaterListeners();
        initServerListListeners();
        initAuthorizationListeners();
        initClientUpdateListeners();

        progressPane.setCenter(progress);

        loginView = new LoginView();
        loginPresenter = (LoginPresenter) loginView.getPresenter();

        settingsView = new SettingsView();
        settingsPresenter = (SettingsPresenter) settingsView.getPresenter();

        downloadView = new DownloadView();
        downloadPresenter = (DownloadPresenter) downloadView.getPresenter();

        showView(loginView.getView());

        launcherUpdater.start();
    }

    private void initUpdaterListeners() {
        launcherUpdater.getUpdateStateProperty().addListener((observable, oldValue, newValue) -> {
            switch (newValue) {
                case RUNNING: {
                    showProgress(true);
                    progress.setStatusText("Проверка версии...");
                    break;
                }
                case NOT_FOUND: {
                    serverListProvider.start();
                    break;
                }
                case FOUND: {
                    progress.setStatusText("Доступна новая версия лаучнера");
                    break;
                }
                case ERROR: {
                    progress.setFailed(true);
                    progress.setStatusText("Невозможно загрузить информацию о версии");
                    break;
                }
                default: {
                    break;
                }
            }
        });
    }

    private void initServerListListeners() {
        serverListProvider.setOnScheduled(event -> progress.setStatusText("Получение списка серверов..."));
        serverListProvider.setOnSucceeded(event -> showProgress(false));
        serverListProvider.setOnFailed(event -> {
            progress.setFailed(true);
            progress.setStatusText("Ошибка получения списка серверов");
        });
    }

    private void initAuthorizationListeners() {
        authorizationProvider.stateProperty().addListener((observable, oldValue, newValue) -> {
            switch (newValue) {
                case REQUESTING: {
                    showProgress(true);
                    progress.setStatusText("Авторизация...");
                    break;
                }
                case SUCCEED: {
                    showProgress(false);
                    break;
                }
                case FAILED: {
                    showProgress(false);
                    break;
                }
                default:
                    break;
            }
        });
    }

    private void initClientUpdateListeners() {
        clientUpdateProvider.stateProperty().addListener((observable, oldValue, newValue) -> {
            switch (newValue) {
                case REQUESTING:
                    showProgress(true);
                    progress.setStatusText("Проверка обновлений...");
                    break;
                case PREPARING:
                    progress.setStatusText("Вычисление размера обновления...");
                    break;
                case DOWNLOADING:
                    showProgress(false);
                    showView(downloadView.getView());
                    break;
                case INSTALLING:
                    log.info("Установка обновлений");
                    break;
                case SUCCESS:
                    log.info("Обновления успешно установлены");
                    break;
                case FAILED:
                    showProgress(true);
                    progress.setFailed(true);
                    progress.setStatusText("Ошибка при обновлении игры");
                    break;
                default:
                    break;
            }
        });
    }

    void showView(Parent view) {
        content.getChildren().clear();
        content.getChildren().add(view);
    }

    void showProgress(boolean show) {
        if (show) {
            contentHolder.setEffect(blur);
        } else {
            contentHolder.setEffect(null);
        }
        progressPane.setVisible(show);
    }
}
