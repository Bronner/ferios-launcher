package ru.ferios.launcher.ui.view.download;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import ru.ferios.launcher.service.ClientUpdateProvider;

import java.net.URL;
import java.util.ResourceBundle;
import javax.inject.Inject;

import static javafx.beans.binding.Bindings.format;

public class DownloadPresenter implements Initializable {

    @FXML
    private Label downloadedLabel;

    @FXML
    private Label speedLabel;

    @FXML
    private Label timeLabel;

    @FXML
    private Label filesCount;

    @FXML
    private ProgressBar progressBar;

    @Inject
    private ClientUpdateProvider clientUpdater;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        speedLabel.textProperty().bind(format("Скорость: %s/c", clientUpdater.speedProperty()));
        filesCount.textProperty()
                  .bind(format("Файлов в очереди: %d", clientUpdater.filesProperty()));
        downloadedLabel.textProperty().bind(
                format("Загружено: %s / %s", clientUpdater.currentProperty(),
                        clientUpdater.totalProperty()));
        progressBar.progressProperty().bind(clientUpdater.percentProperty());
    }
}
