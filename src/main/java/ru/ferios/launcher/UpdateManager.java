package ru.ferios.launcher;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import ru.ferios.commons.downloader.DownloadUrl;
import ru.ferios.commons.downloader.downloads.Downloadable;
import ru.ferios.launcher.domain.ResourceObject;
import ru.ferios.launcher.domain.Server;
import ru.ferios.launcher.domain.AssetIndex;
import ru.ferios.launcher.domain.ExtractRules;
import ru.ferios.launcher.util.MinecraftUtil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.annotation.Nullable;

import static org.apache.commons.lang3.Validate.isTrue;
import static org.apache.commons.lang3.Validate.notNull;
import static ru.ferios.launcher.util.json.JsonHelper.fromJson;

@Slf4j
public class UpdateManager {

    private final AssetsManager am;

    public UpdateManager() {
        this.am = new AssetsManager();
    }

    public List<Downloadable> getUpdates(Server server) {
        notNull(server, "server");
        List<Downloadable> list = new ArrayList<>();

        list.add(getClientUpdates(server));
        list.addAll(getLibrariesUpdates(server));
        list.addAll(getModsUpdates(server));
        list.addAll(am.getResourceFiles(MinecraftUtil.getWorkingDirectory(),
                am.checkResources(server, true)));
        list.addAll(getCommonFiles());
        return list;
    }

    @Nullable
    private Downloadable getClientUpdates(Server server) {
        String clientName = server.getClientName();
        ResourceObject remoteClient = getRemoteClient(clientName);
        server.setClient(remoteClient);
        File clientDir = new File(MinecraftUtil.getWorkingDirectory(), Properties.CLIENT_DIR);
        File localClient = new File(clientDir, remoteClient.getName());

        if (remoteClient.getSize() != localClient.length()) {
            return new Downloadable(localClient, new DownloadUrl(remoteClient.getPath(),
                    new String[]{"http://res.ferios.ru/game/versions/",
                            "http://s3.amazonaws.com/Minecraft.Download/versions/" + server
                                    .getClientVersion() + "/"}));
        }
        return null;
    }

    private Collection<Downloadable> getLibrariesUpdates(Server server) {
        String libListName = server.getLibListName();
        Collection<ResourceObject> remoteLibs = getRemoteLibList(libListName);
        server.setLibList(remoteLibs);
        Collection<Downloadable> downloads = new HashSet<>();
        File libsDir = MinecraftUtil.getFile(Properties.LIB_DIR);
        File localLib;
        Downloadable downloadable;
        DownloadUrl url;

        for (ResourceObject remoteLib : remoteLibs) {
            localLib = new File(libsDir, remoteLib.getPath());
            if (localLib.length() != remoteLib.getSize()) {
                url = new DownloadUrl(remoteLib.getPath(), Properties.LIBRARIES_REPO);
                downloadable = new Downloadable(localLib, url);
                downloads.add(downloadable);
            }
        }
        return downloads;
    }

    private Collection<Downloadable> getModsUpdates(Server server) {
        String modListName = server.getModListName();
        Collection<ResourceObject> remoteMods = getRemoteModList(modListName);
        server.setModList(remoteMods);
        Collection<Downloadable> downloads = new HashSet<>();
        File modsDir = new File(MinecraftUtil.getWorkingDirectory(), "mods_" + modListName);
        File localMod;
        Downloadable downloadable;
        DownloadUrl url;

        for (ResourceObject remoteMod : remoteMods) {
            localMod = new File(modsDir, remoteMod.getName());
            if (localMod.length() != remoteMod.getSize()) {
                url = new DownloadUrl(remoteMod.getPath(), Properties.MODS_REPO);
                downloadable = new Downloadable(localMod, url);
                downloads.add(downloadable);
            }
        }
        return downloads;
    }

    private Collection<Downloadable> getCommonFiles() {
        Set<Downloadable> set = new HashSet<>();
        //        File gameDir = MinecraftUtil.getWorkingDirectory();
        //
        //        for (String s : commons)
        //        {
        //            File file = new File(gameDir, s);
        //            if (!file.exists())
        //            {
        //                set.add(new Downloadable(Repository.COMMONS, s, file, false));
        //            }
        //        }

        return set;
    }

    private ResourceObject getRemoteClient(String name) {
        log.info("Получение информации о клиенте");
        notNull(name, "client name");
        HttpRequest request = HttpRequest.get(Properties.API_CLIENT + "/" + name).trustAllCerts();
        isTrue(request.ok(), "Невозможно получить информацию о клиенте: " + request.message());
        return notNull(fromJson(request.reader(), ResourceObject.class), "remote client");
    }

    private Collection<ResourceObject> getRemoteLibList(String libListName) {
        log.info("Загрузка информации о библиотеках");
        notNull(libListName, "libs name");
        HttpRequest request =
                HttpRequest.get(Properties.API_LIBS + "/" + libListName).trustAllCerts();
        isTrue(request.ok(), "Невозможно получить информацию о библиотеках: " + request.message());
        return notNull(
                fromJson(request.reader(), new TypeToken<Collection<ResourceObject>>() {}.getType()),
                "remote libs");
    }

    private Collection<ResourceObject> getRemoteModList(String modListName) {
        log.info("Загрузка информации о модах");
        notNull(modListName, "modListName");
        HttpRequest request =
                HttpRequest.get(Properties.API_MODS + "/" + modListName).trustAllCerts();
        isTrue(request.ok(), "Ошибка при загрузке информации о модах: " + request.message());
        return notNull(
                fromJson(request.reader(), new TypeToken<Collection<ResourceObject>>() {}.getType()),
                "remote mods");
    }

    public void installUpdates(Server server) throws IOException {
        unpackNatives(server, false);
        reconstructAssets(server);
    }

    private void unpackNatives(Server server, boolean force) throws IOException {
        log.info("Распаковка библиотек");
        File libsDir = MinecraftUtil.getFile(Properties.LIB_DIR);
        File nativesDir = new File(MinecraftUtil.getFile(Properties.CLIENT_DIR), "natives");
        for (ResourceObject library : server.getLibList()) {
            if (!library.getPath().contains("natives")) {
                continue;
            }

            File libFile = new File(libsDir, library.getPath());

            ExtractRules extractRules = new ExtractRules("META-INF/");
            try (ZipFile zip = new ZipFile(libFile)) {
                Enumeration<? extends ZipEntry> entries = zip.entries();

                while (entries.hasMoreElements()) {
                    ZipEntry entry = entries.nextElement();
                    if (extractRules.shouldExtract(entry.getName())) {
                        File targetFile = new File(nativesDir, entry.getName());
                        if (!force && targetFile.exists() && targetFile.length() == entry
                                .getSize()) {
                            continue;
                        }
                        if (targetFile.getParentFile() != null) {
                            targetFile.getParentFile().mkdirs();
                        }
                        if (!entry.isDirectory()) {
                            BufferedInputStream inputStream =
                                    new BufferedInputStream(zip.getInputStream(entry));

                            byte[] buffer = new byte[2048];
                            FileOutputStream outputStream = new FileOutputStream(targetFile);
                            BufferedOutputStream bufferedOutputStream =
                                    new BufferedOutputStream(outputStream);
                            try {
                                int length;
                                while ((length = inputStream.read(buffer, 0, buffer.length))
                                        != -1) {
                                    bufferedOutputStream.write(buffer, 0, length);
                                }
                            } finally {
                                IOUtils.closeQuietly(bufferedOutputStream);
                                IOUtils.closeQuietly(outputStream);
                                IOUtils.closeQuietly(inputStream);
                            }
                        }
                    }
                }
            }
        }
    }

    private File reconstructAssets(Server server) throws IOException {
        File assetsDir = MinecraftUtil.getFile("assets");
        File indexDir = new File(assetsDir, "indexes");
        File objectDir = new File(assetsDir, "objects");
        File indexFile = new File(indexDir, server.getAssets() + ".json");
        File virtualRoot = new File(new File(assetsDir, "virtual"), server.getAssets());

        if (!indexFile.isFile()) {
            log.warn("No assets index file " + virtualRoot + "; can't reconstruct assets");
            return virtualRoot;
        }

        AssetIndex index = fromJson(FileUtils.readFileToString(indexFile, StandardCharsets.UTF_8),
                AssetIndex.class);

        if (index.isVirtual()) {
            for (Map.Entry<String, AssetIndex.AssetObject> entry : index.getObjects().entrySet()) {
                File target = new File(virtualRoot, entry.getKey());
                File original = new File(objectDir, entry.getValue().getFilename());

                if (!target.isFile()) {
                    FileUtils.copyFile(original, target, false);
                }
            }

            //FileUtils.writeStringToFile(new File(virtualRoot, ".lastused"), this.dateAdapter.serializeToString(new java.util.Date()));
        }

        return virtualRoot;
    }

    private void cleanUp(File dir, Set<String> files) {
        log.info("Немного прибераемся...");
        if (!dir.exists()) {
            return;
        }

        Collection<File> allFiles =
                FileUtils.listFilesAndDirs(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
        Set<File> remoteFiles = new HashSet<>();

        for (String fileName : files) {
            File file = new File(dir, fileName);
            remoteFiles.add(file);
        }

        allFiles.removeAll(remoteFiles);

        for (File userFile : allFiles) {
            userFile.delete();
            File parent = userFile.getParentFile();
            if (parent.isDirectory() && parent.list().length == 0) {
                parent.delete();
            }
        }
    }
}
