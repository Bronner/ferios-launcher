package ru.ferios.launcher;

import com.google.common.eventbus.EventBus;

public final class EventManager {

    private static final EventBus eventBus = new EventBus();

    public static void post(Object event) {
        eventBus.post(event);
    }

    public static void register(Object listener) {
        eventBus.register(listener);
    }

    public static void unregister(Object listener) {
        eventBus.unregister(listener);
    }
}
