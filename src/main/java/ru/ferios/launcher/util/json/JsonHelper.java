package ru.ferios.launcher.util.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Reader;
import java.lang.reflect.Type;

public class JsonHelper {

    private static final Gson gson;

    static {
        gson = new GsonBuilder()
                .registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory())
                .enableComplexMapKeySerialization()
                .setPrettyPrinting()
                .create();

    }

    public static String toJson(Object o) {
        return gson.toJson(o);
    }

    public static <T> T fromJson(Reader json, Class<T> type) {
        return gson.fromJson(json, type);
    }

    public static <T> T fromJson(Reader json, Type type) {
        return gson.fromJson(json, type);
    }

    public static <T> T fromJson(String json, Type type) {
        return gson.fromJson(json, type);
    }
}
