package ru.ferios.launcher.util;

import lombok.extern.slf4j.Slf4j;
import ru.ferios.commons.utils.Platform.Arch;

@Slf4j
public class Debugger {

    public static void printSystemInfo() {
        log.info("OS: {} ({}-bit), Version {}",
                System.getProperty("os.name"),
                Arch.CURRENT.asString(),
                System.getProperty("os.version"));
        log.info("Java: {} ({}-bit) by {}",
                System.getProperty("java.version"),
                System.getProperty("sun.arch.data.model"),
                System.getProperty("java.vendor"));
    }
}
