package ru.ferios.launcher.util;

import lombok.extern.slf4j.Slf4j;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import javax.imageio.ImageIO;

@Slf4j
public final class Resource {

    public static BufferedImage getImage(int w, int h, String name) {
        BufferedImage resizedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = resizedImage.createGraphics();
        g.setColor(new Color(0, 0, 0, 0));
        g.drawImage(getImage(name), 0, 0, w, h, null);
        g.dispose();
        return resizedImage;
    }

    public static BufferedImage getImage(String name) {
        InputStream is = get("images/" + name);
        BufferedImage img = null;
        try {
            img = ImageIO.read(is);
            is.close();
        } catch (Exception e) {
            log.error("Невозможно получить изображение: " + name);
        }
        return img;
    }

    public static InputStream get(String name) {
        name = "/assets/" + name;
        InputStream is = Resource.class.getResourceAsStream(name);
        if (is == null) {
            throw new NullPointerException("Ресурс не найден: " + name);
        }
        return is;
    }
}
