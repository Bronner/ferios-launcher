/*
 * This file is part of ${name}, ${description}
 *
 * Copyright (c) ${year} ${organization} <${url}/>, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

package ru.ferios.launcher.util.java;

public interface eJavaNetAccess {

    /**
     * return the URLClassPath belonging to the given loader
     */
    eURLClassPath geteURLClassPath(eURLClassLoader u);
}
