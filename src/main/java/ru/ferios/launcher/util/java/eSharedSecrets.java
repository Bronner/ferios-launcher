/*
 * This file is part of ${name}, ${description}
 *
 * Copyright (c) ${year} ${organization} <${url}/>, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */
package ru.ferios.launcher.util.java;

import sun.misc.SharedSecrets;

/**
 * A repository of "shared secrets", which are a mechanism for calling
 * implementation-private methods in another package without using reflection. A
 * package-private class implements a public interface and provides the ability
 * to call package-private methods within that package; the object implementing
 * that interface is provided through a third package to which access is
 * restricted. This framework avoids the primary disadvantage of using
 * reflection for this purpose, namely the loss of compile-time checking.
 */
public class eSharedSecrets extends SharedSecrets {

    private static eJavaNetAccess javaNetAccess;

    public static eJavaNetAccess geteJavaNetAccess() {
        return javaNetAccess;
    }

    public static void seteJavaNetAccess(eJavaNetAccess jna) {
        javaNetAccess = jna;
    }
}
