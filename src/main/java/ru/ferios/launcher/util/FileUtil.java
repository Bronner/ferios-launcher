package ru.ferios.launcher.util;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FileUtil {

    public static final String DEFAULT_CHARSET = "UTF-8";

    public static Charset getCharset() {
        try {
            return Charset.forName("UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDigest(File file, String algorithm, int hashLength) {
        DigestInputStream stream = null;
        int read;
        try {
            stream = new DigestInputStream(new FileInputStream(file),
                    MessageDigest.getInstance(algorithm)
            );
            byte[] buffer = new byte[65536];
            //int read;
            do {
                read = stream.read(buffer);
            } while (

                    read > 0);
        } catch (Exception ignored) {
            return null;
        } finally {
            close(stream);
        }

        return String.format("%1$0" + hashLength + "x",
                new Object[]{new java.math.BigInteger(1, stream.getMessageDigest()
                                                               .digest()
                )}
        );
    }

    private static void close(Closeable a) {
        try {
            a.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String copyAndDigest(InputStream inputStream, OutputStream outputStream, String algorithm,
            int hashLength)
            throws IOException {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {

            throw new RuntimeException("Missing Digest. " + algorithm, e);
        }
        byte[] buffer = new byte[65536];
        try {
            int read = inputStream.read(buffer);
            while (read >= 1) {
                digest.update(buffer, 0, read);
                outputStream.write(buffer, 0, read);
                read = inputStream.read(buffer);
            }
        } finally {
            close(inputStream);
            close(outputStream);
        }
        return String.format("%1$0" + hashLength + "x",
                new Object[]{new java.math.BigInteger(1, digest.digest())}
        );
    }

    public static String getChecksum(File file, String algorithm) {
        if (file == null) {
            return null;
        }
        if (!file.exists()) {
            return null;
        }
        byte[] b = createChecksum(file, algorithm);
        if (b == null) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        for (byte cb : b) {
            result.append(Integer.toString((cb & 0xFF) + 256, 16)
                                 .substring(1)
            );
        }
        return result.toString();
    }

    private static byte[] createChecksum(File file, String algorithm) {
        InputStream fis = null;
        try {
            fis = new BufferedInputStream(new FileInputStream(file));

            byte[] buffer = new byte[1024];
            MessageDigest complete = MessageDigest.getInstance(algorithm);
            int numRead;
            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    complete.update(buffer, 0, numRead);
                }
            } while (numRead != -1);
            return complete.digest();
        } catch (Exception e) {
            return null;
        } finally {
            close(fis);
        }
    }

    public static File getRunningJar() {
        try {
            return new File(java.net.URLDecoder.decode(FileUtil.class
                    .getProtectionDomain().getCodeSource().getLocation()
                    .getPath(), "UTF-8"
            )
            );
        } catch (java.io.UnsupportedEncodingException e) {
            throw new RuntimeException("Cannot get running file!", e);
        }
    }

    public static void deleteFile(File file) {
        boolean onExit = !file.delete();

        if (onExit) {
            file.deleteOnExit();
            return;
        }

        File parent = file.getParentFile();
        if (parent == null) {
            return;
        }
        File[] list = parent.listFiles();
        if ((list == null) || (list.length > 0)) {
            return;
        }
        deleteFile(parent);
    }

    public static boolean createFolder(String dir) throws IOException {
        if (dir == null) {
            return false;
        }
        return createFolder(new File(dir));
    }

    public static boolean createFolder(File dir) throws IOException {
        if (dir == null) {
            throw new NullPointerException();
        }
        if (dir.isDirectory()) {
            return false;
        }
        if (!dir.mkdirs()) {
            throw new IOException("Cannot create folders: " +
                    dir.getAbsolutePath()
            );
        }
        if (!dir.canWrite()) {
            throw new IOException("Ceated directory is not accessible: " +
                    dir.getAbsolutePath()
            );
        }
        return true;
    }

    public static boolean folderExists(String path) {
        if (path == null) {
            return false;
        }
        File folder = new File(path);
        return folder.isDirectory();
    }

    public static boolean fileExists(String path) {
        if (path == null) {
            return false;
        }
        File file = new File(path);
        return file.isFile();
    }

    public static void createFile(String file) throws IOException {
        createFile(new File(file));
    }

    public static void createFile(File file) throws IOException {
        if (file.isFile()) {
            return;
        }
        if (file.getParentFile() != null) {
            file.getParentFile().mkdirs();
        }
        if (!file.createNewFile()) {
            throw new IOException("Cannot create file, or it was created during runtime: " + file.getAbsolutePath());
        }
    }

    //    public static void unZip(File zip, File folder, boolean replace) throws IOException {
    //        createFolder(folder);
    //
    //        ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zip)));
    //
    //        byte[] buffer = new byte[1024];
    //        ZipEntry ze;
    //        while ((ze = zis.getNextEntry()) != null) {
    //            ZipEntry ze;
    //            String fileName = ze.getLogin();
    //            File newFile = new File(folder, fileName);
    //
    //            if ((!replace) && (newFile.isFile())) {
    //                U.log(new Object[]{"[UnZip] File exists:", newFile.getAbsoluteFile()});
    //            } else {
    //                U.log(new Object[]{"[UnZip]", newFile.getAbsoluteFile()});
    //                createFile(newFile);
    //
    //                OutputStream fos = new BufferedOutputStream(new FileOutputStream(
    //                        newFile
    //                )
    //                );
    //
    //                int len;
    //                while ((len = zis.read(buffer)) > 0) {
    //                    int len;
    //                    fos.write(buffer, 0, len);
    //                }
    //
    //                fos.close();
    //            }
    //        }
    //        zis.closeEntry();
    //        zis.close();
    //    }
    //
    //    public static void removeFromZip(File zipFile, List<String> files) throws IOException {
    //        File tempFile = File.createTempFile(zipFile.getLogin(), null);
    //        tempFile.delete();
    //        tempFile.deleteOnExit();
    //
    //        boolean renameOk = zipFile.renameTo(tempFile);
    //        if (!renameOk) {
    //            throw new IOException("Could not rename the file " +
    //                                  zipFile.getAbsolutePath() + " to " +
    //                                  tempFile.getAbsolutePath()
    //            );
    //        }
    //        byte[] buf = new byte[1024];
    //
    //        ZipInputStream zin = new ZipInputStream(new BufferedInputStream(
    //                new FileInputStream(tempFile)
    //        )
    //        );
    //        ZipOutputStream zout = new ZipOutputStream(new BufferedOutputStream(
    //                new FileOutputStream(zipFile)
    //        )
    //        );
    //
    //        ZipEntry entry = zin.getNextEntry();
    //        while (entry != null) {
    //            String login = entry.getLogin();
    //
    //            if (!files.contains(login)) {
    //                zout.putNextEntry(new ZipEntry(login));
    //                int len;
    //                while ((len = zin.read(buf)) > 0) {
    //                    int len;
    //                    zout.write(buf, 0, len);
    //                }
    //            }
    //
    //            entry = zin.getNextEntry();
    //        }
    //
    //        zin.close();
    //        zout.close();
    //        tempFile.delete();
    //    }

    public static String getResource(URL resource) throws IOException {
        return getResource(resource, "UTF-8");
    }

    public static String getResource(URL resource, String charset) throws IOException {
        InputStream is = new BufferedInputStream(resource.openStream());

        InputStreamReader reader = new InputStreamReader(is, charset);

        StringBuilder b = new StringBuilder();
        while (reader.ready()) {
            b.append((char) reader.read());
        }
        reader.close();

        return b.toString();
    }

    //    public static String getFolder(URL url, String separator) {
    //        String[] folders = url.toString().split(separator);
    //        String s = "";
    //
    //        for (int i = 0; i < folders.length - 1; i++) {
    //            s = s + folders[i] + separator;
    //        }
    //        return s;
    //    }
    //
    //    public static String getFolder(URL url) {
    //        return getFolder(url, "/");
    //    }

    //    private static File getNeighborFile(File file, String filename) {
    //        File parent = file.getParentFile();
    //        if (parent == null) {
    //            parent = new File("/");
    //        }
    //        return new File(parent, filename);
    //    }
    //
    //    public static File getNeighborFile(String filename) {
    //        return getNeighborFile(getRunningJar(), filename);
    //    }

    //    public byte[] getFile(File archive, String requestedFile) throws IOException {
    //        ByteArrayOutputStream out = new ByteArrayOutputStream();
    //        ZipInputStream in = null;
    //        try {
    //            in = new ZipInputStream(new FileInputStream(archive));
    //            ZipEntry entry;
    //            while ((entry = in.getNextEntry()) != null) {
    //                ZipEntry entry;
    //                if (entry.getLogin().equals(requestedFile)) {
    //                    byte[] buf = new byte[1024];
    //                    int len;
    //                    while ((len = in.read(buf)) > 0) {
    //                        int len;
    //                        out.write(buf, 0, len);
    //                    }
    //                }
    //            }
    //        } finally {
    //            if (in != null)
    //                in.close();
    //            out.close();
    //        }
    //        return out.toByteArray();
    //    }
}
