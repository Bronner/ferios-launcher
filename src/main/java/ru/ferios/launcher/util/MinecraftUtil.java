package ru.ferios.launcher.util;

import ru.ferios.commons.utils.Platform;
import ru.ferios.launcher.Properties;

import java.io.File;

public class MinecraftUtil {

    private static File workingDir;

    public static File getOptionsFile() {
        return getFile("options.txt");
    }

    public static File getFile(String name) {
        return new File(getWorkingDirectory(), name);
    }

    public static File getWorkingDirectory() {
        //Custom user folders
        if (workingDir == null) {
            workingDir = getDefaultWorkingDirectory();
        }
        return workingDir;
    }

    public static File getDefaultWorkingDirectory() {
        return getSystemRelatedFile(Properties.FOLDER_NAME + File.separator);
    }

    public static File getSystemRelatedFile(String path) {
        String userHome = System.getProperty("user.home", ".");
        switch (Platform.CURRENT) {
            case WINDOWS:
                String applicationData = System.getenv("APPDATA");
                String folder = applicationData != null ? applicationData : userHome;
                return new File(folder, path);
            case OSX:
                return new File(userHome, "Library/Application Support/" + path);
            default:
                return new File(userHome, "." + path.toLowerCase());
        }
    }
}
