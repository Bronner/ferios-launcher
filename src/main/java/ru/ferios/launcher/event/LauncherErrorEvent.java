package ru.ferios.launcher.event;

import lombok.Getter;

import javax.annotation.Nullable;

@Getter
public class LauncherErrorEvent extends UserNotificationEvent {

    @Nullable
    private Throwable exception;

    public LauncherErrorEvent(String message, @Nullable Throwable exception) {
        super(message);
        this.exception = exception;
    }
}
