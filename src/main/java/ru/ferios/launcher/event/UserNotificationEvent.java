package ru.ferios.launcher.event;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserNotificationEvent extends LauncherEvent {

    private String message;

    public UserNotificationEvent(String message) {
        this.message = message;
    }
}
