package ru.ferios.launcher;

import lombok.Getter;

import static org.apache.commons.lang3.Validate.matchesPattern;
import static org.apache.commons.lang3.Validate.notNull;

public class LauncherVersion {

    @Getter
    private short major = 0;

    @Getter
    private short minor = 0;

    @Getter
    private short build = 0;

    public LauncherVersion(String version) {
        notNull(version, "Не указана версия лаунчера");
        matchesPattern(version, "^[0-9]+\\.[0-9]+\\.[0-9]+$", "Указанная "
                + "версия не "
                + "соответствует "
                + "шаблону minor"
                + ".major.build (build is optional)");

        String[] args = version.split("\\.");
        major = Short.parseShort(args[0]);
        minor = Short.parseShort(args[1]);
        build = Short.parseShort(args[2]);
    }

    /**
     * Проверяет актуальность текущей версии,
     * по отношения к переданной в параметрах
     *
     * @param version Версия, с которой нужно сравнить
     * @return True если текущая версия актуальна, иначе false
     */
    public boolean isUpToDate(LauncherVersion version) {
        if (major < version.getMajor()) {
            return false;
        }
        if (major > version.getMajor()) {
            return true;
        }

        if (minor < version.getMinor()) {
            return false;
        }
        if (minor > version.getMinor()) {
            return true;
        }

        if (build < version.getBuild()) {
            return false;
        }
        if (build > version.getBuild()) {
            return true;
        }

        return true;
    }

    @Override
    public String toString() {
        return major + "." + minor + "." + build;
    }
}
