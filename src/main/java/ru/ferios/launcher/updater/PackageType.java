package ru.ferios.launcher.updater;

import ru.ferios.launcher.util.FileUtil;

enum PackageType {
    EXE,
    JAR;

    public static boolean isCurrent(PackageType pt) {
        return getCurrent() == pt;
    }

    public static PackageType getCurrent() {
        if (isWrapped()) {
            return EXE;
        }
        return JAR;
    }

    private static boolean isWrapped() {
        return FileUtil.getRunningJar().toString().endsWith(".exe");
    }

    public String toLowerCase() {
        return this.name().toLowerCase();
    }
}
