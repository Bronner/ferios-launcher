package ru.ferios.launcher.updater;

import com.github.kevinsawicki.http.HttpRequest;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.Validate;
import ru.ferios.launcher.EventManager;
import ru.ferios.launcher.Launcher;
import ru.ferios.launcher.LauncherVersion;
import ru.ferios.launcher.Properties;
import ru.ferios.launcher.domain.LauncherInfo;
import ru.ferios.launcher.event.LauncherErrorEvent;
import ru.ferios.launcher.updater.LauncherUpdater.State;
import ru.ferios.launcher.util.FileUtil;
import ru.ferios.launcher.util.json.JsonHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

@Slf4j
public class LauncherUpdater extends Service<State> {

    private LauncherVersion remoteVersion;

    private ObjectProperty<State> updateState = new SimpleObjectProperty<>(State.UNKNOWN);

    public LauncherUpdater() {

        if (!PackageType.isCurrent(PackageType.JAR)) {
            File oldfile = LauncherUpdater.getTempFile();
            if (oldfile.delete()) {
                log.info("Old version has been deleted");
            }
        }
        log.info("Тип лаунчера: {}", PackageType.getCurrent());
    }

    private static File getTempFile() {
        return getTempFileFor(PackageType.getCurrent());
    }

    private static File getTempFileFor(PackageType pt) {
        return new File(getFileFor(pt).getAbsolutePath() + ".replace");
    }

    public static File getFileFor(PackageType pt) {
        Validate.notNull(pt);

        switch (pt) {
            case EXE:
            case JAR:
                return FileUtil.getRunningJar();
            default:
                throw new IllegalArgumentException("Unknown PackageType!");
        }
    }

    public ReadOnlyObjectProperty<State> getUpdateStateProperty() {
        return updateState;
    }

    @Override
    protected Task<State> createTask() {
        return new Task<State>() {
            @Override
            protected void scheduled() {
                updateState.setValue(LauncherUpdater.State.RUNNING);
            }

            @Override
            protected void failed() {
                updateState.setValue(LauncherUpdater.State.ERROR);
                EventManager.post(
                        new LauncherErrorEvent("Ошибка при проверке версии лаунчера", getException()));
            }

            @Override
            protected void succeeded() {
                if (getValue().equals(LauncherUpdater.State.NOT_FOUND)) {
                    log.info("Обновление лаучера не требуется");
                } else if (getValue().equals(LauncherUpdater.State.FOUND)) {
                    log.info("Найдена новая версия лаунчера. Требуется обновление.");
                }
                updateState.setValue(getValue());
            }

            @Override
            protected LauncherUpdater.State call() throws Exception {
                return checkUpdate()
                        ? LauncherUpdater.State.FOUND
                        : LauncherUpdater.State.NOT_FOUND;
            }
        };
    }

    /**
     * Проверяет версию лаунчера.
     *
     * @return true - если имеется более новая версия лаунчера
     */
    private boolean checkUpdate() {
        log.info("Проверка версии лаунчера");
        HttpRequest request = HttpRequest.get(Properties.API_LAUNCHER).trustAllCerts();
        Validate.isTrue(request.ok(), request.message());
        LauncherInfo launcherInfo = JsonHelper.fromJson(request.reader(), LauncherInfo.class);
        remoteVersion = new LauncherVersion(launcherInfo.getVersion());
        return !Launcher.getVersion().isUpToDate(remoteVersion);
    }

    //    @Override protected void scheduled() {
    //        authController.setDisabled(true);
    //        authController.showNotice("Проверка версии лаунчера...");
    //        authController.getProgressIndicator().setVisible(true);
    //    }
    //
    //    @Override protected void succeeded() {
    //        if (!getValue()) {
    //            authController.getProgressIndicator().setVisible(false);
    //            authController.getJoinButton().setText("Обновить");
    //            authController.getJoinButton().setOnAction(e -> {
    //                authController.setDisabled(true);
    //                startUpdate();
    //            });
    //
    //            authController.showNotice("Доступна новая версия лаунчера");
    //        } else {
    //            authController.showNotice("");
    //            authController.getProgressIndicator().setVisible(false);
    //        }
    //
    //        uiLock.arriveAndAwaitAdvance();
    //        authController.setDisabled(false);
    //    }

    public void startUpdate() {
        File temp = getUpdateFile();
        temp.deleteOnExit();
        String fileName =
                Properties.FILE_NAME + "-" + remoteVersion.toString() + "." + PackageType.getCurrent()
                                                                                         .toLowerCase();
        //        Downloadable update = new Downloadable(Repository.LAUNCHER, fileName, temp, true);
        //        dm.addLisneter(new DownloadListener()
        //        {
        //            public void onDownloadStart(DownloadManager dm)
        //            {
        //            }
        //
        //            public void onProgressChanged(DownloadManager dm)
        //            {
        //            }
        //
        //            public void onDownloadComplete(DownloadManager dm)
        //            {
        //                dm.removeListener(this);
        //                installUpdate();
        //            }
        //
        //            public void onDownloadError(DownloadManager dm)
        //            {
        //                dm.removeListener(this);
        //                state = UpdaterState.ERROR;
        //            }
        //        });
        //        dm.addDownload(update);
        //        dm.startDownload();
    }

    public static File getUpdateFile() {
        return getUpdateFileFor(PackageType.getCurrent());
    }

    public static File getUpdateFileFor(PackageType pt) {
        return new File(getFileFor(pt).getAbsolutePath() + ".update");
    }

    private void installUpdate() {
        FileInputStream in = null;
        FileOutputStream out = null;
        try {
            File target = getFile();
            File source = getUpdateFile();
            source.deleteOnExit();
            in = new FileInputStream(source);
            out = new FileOutputStream(target);
            IOUtils.copy(in, out);
            out.close();
            in.close();
            String java =
                    System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
            Runtime.getRuntime().exec(new String[]{java, "-jar", target.toString()});
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(out);
        }
    }

    public static File getFile() {
        return getFileFor(PackageType.getCurrent());
    }

    public enum State {
        UNKNOWN,
        RUNNING,
        FOUND,
        NOT_FOUND,
        ERROR
    }
}
