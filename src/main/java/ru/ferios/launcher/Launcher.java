package ru.ferios.launcher;

import com.google.common.eventbus.Subscribe;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import ru.ferios.launcher.event.LauncherCloseEvent;
import ru.ferios.launcher.event.LauncherErrorEvent;
import ru.ferios.launcher.ui.view.launcher.LauncherView;
import ru.ferios.launcher.util.Debugger;
import ru.ferios.launcher.util.GuiUtil;

@Slf4j
public final class Launcher extends Application {

    @Getter
    private static final LauncherVersion version = new LauncherVersion("3.0.0");

    public Launcher() throws Exception {

        Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
            log.error(e.getMessage(), e);
            //            GuiUtil.showMessageDialog(e.getMessage(), 1);
        });
        EventManager.register(this);
    }

    public static void main(String[] args) {
        log.info("Ferios Launcher {}", version);
        Debugger.printSystemInfo();
        try {
            launch(args);
        } catch (Exception e) {
            log.error("Невозможно запустить приложение", e);
            GuiUtil.showMessageDialog("Невозможно запустить приложение", 1);
        }
    }

    @Override
    public void stop() throws Exception {
        EventManager.post(new LauncherCloseEvent());
        ThreadManager.shutdown();
        super.stop();
    }

    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(new LauncherView().getView());
        scene.getStylesheets().add("css/style.css");
        stage.setScene(scene);
        stage.setTitle("Ferios Launcher");
        stage.centerOnScreen();
        stage.sizeToScene();
        stage.setResizable(false);
        stage.show();
    }

    @Subscribe
    public void onError(LauncherErrorEvent event) {
        log.error(event.getMessage(), event.getException());
    }
}
