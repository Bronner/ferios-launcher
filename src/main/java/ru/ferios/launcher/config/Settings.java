package ru.ferios.launcher.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.annotations.Expose;
import lombok.Data;
import org.apache.commons.io.FileUtils;
import ru.ferios.launcher.util.MinecraftUtil;
import ru.ferios.launcher.util.json.LowerCaseEnumTypeAdapterFactory;

import java.io.File;
import java.io.IOException;

@Data
public class Settings {

    private static final Gson gson;

    private static Settings INSTANCE;

    private static File file;

    static {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory());
        //builder.registerTypeAdapter(Date.class, new DateTypeAdapter());
        builder.enableComplexMapKeySerialization();
        builder.setPrettyPrinting();
        builder.excludeFieldsWithoutExposeAnnotation();
        gson = builder.create();

    }

    @Expose
    private boolean fullscreen = false;

    @Expose
    private boolean savePassword = false;

    @Expose
    private boolean autologin = false;

    private boolean offline = false;

    @Expose
    private int[] sizes = {925, 530};

    @Expose
    private int memory = 1024;

    @Expose
    private String token = null;

    @Expose
    private String username = null;

    @Expose
    private int server;

    private Settings() {
    }

    public static File getFile() throws IOException {
        if (file == null) {
            file = new File(MinecraftUtil.getFile("launcher"), "settings.json");
            if (file.isDirectory()) {
                file.delete();
            }
        }
        return file;
    }

    public static Settings getInstance() {
        if (INSTANCE == null) {
            try {
                String json = FileUtils.readFileToString(getFile());
                JsonElement obj = new JsonParser().parse(json);
                INSTANCE = gson.fromJson(obj, Settings.class);
            } catch (IOException e) {
                INSTANCE = new Settings();
                getInstance().saveSettings();
            }
        }
        return INSTANCE;
    }

    public void saveSettings() {
        try {
            String json = gson.toJson(this);
            FileUtils.writeStringToFile(getFile(), json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
