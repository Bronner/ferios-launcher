package ru.ferios.launcher.service;

import com.google.common.eventbus.Subscribe;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;
import org.apache.commons.lang3.Validate;
import ru.ferios.commons.downloader.DownloadManager;
import ru.ferios.commons.downloader.TotalProgress;
import ru.ferios.commons.downloader.listeners.DownloadListener;
import ru.ferios.launcher.EventManager;
import ru.ferios.launcher.ThreadManager;
import ru.ferios.launcher.UpdateManager;
import ru.ferios.launcher.domain.Server;
import ru.ferios.launcher.event.LauncherCloseEvent;
import ru.ferios.launcher.event.LauncherErrorEvent;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static ru.ferios.commons.downloader.util.UnitHelper.toHumanReadableByteCount;

public class ClientUpdateProvider {

    private ObjectProperty<State> state = new SimpleObjectProperty<>();

    private StringProperty total = new SimpleStringProperty("Неизвестно");

    private StringProperty current = new SimpleStringProperty();

    private StringProperty speed = new SimpleStringProperty();

    private IntegerProperty files = new SimpleIntegerProperty();

    private DoubleProperty percent = new SimpleDoubleProperty(-1);

    private DownloadManager downloadManager;

    private UpdateManager updateManager;

    public ClientUpdateProvider() {
        downloadManager = new DownloadManager();
        updateManager = new UpdateManager();
        EventManager.register(new Object() {
            @Subscribe
            public void onClose(LauncherCloseEvent event) {
                downloadManager.stop();
            }
        });
    }

    public void checkUpdates(Server server) {
        Validate.notNull(server, "server");

        setState(State.REQUESTING);

        final Request request = new Request(server);
        request.setOnSucceeded(event -> downloadManager.start());
        request.setOnFailed(event -> setState(State.FAILED));

        final Install install = new Install(server);
        install.setOnScheduled(event -> setState(State.INSTALLING));
        install.setOnSucceeded(event -> setState(State.SUCCESS));
        install.setOnFailed(event -> setState(State.FAILED));

        downloadManager.addLisneter(new DownloadListener() {
            @Override
            public void onDownloadStart(DownloadManager downloadManager) {
                Platform.runLater(() -> setState(State.DOWNLOADING));
            }

            @Override
            public void onProgressChanged(TotalProgress progress) {
                Platform.runLater(() -> {
                    speed.setValue(toHumanReadableByteCount(progress.getAverageSpeed(), true));
                    files.setValue(progress.getRemainingFiles());
                    current.setValue(toHumanReadableByteCount(progress.getCurrent(), true));
                    if (progress.getTotal() > 0) {
                        total.setValue(toHumanReadableByteCount(progress.getTotal(), true));
                        percent.setValue(
                                (double) progress.getCurrent() / (double) progress.getTotal());
                    }
                });
            }

            @Override
            public void onDownloadComplete(DownloadManager downloadManager) {
                ThreadManager.execute(install);
                downloadManager.removeListener(this);
            }

            @Override
            public void onDownloadError(DownloadManager downloadManager) {
                Platform.runLater(() -> setState(State.FAILED));
            }

            @Override
            public void onDownloadPrepare(DownloadManager downloadManager) {
                Platform.runLater(() -> setState(State.PREPARING));
            }

            @Override
            public void onDownloadCancelled(DownloadManager downloadManager) {

            }
        });

        ThreadManager.execute(request);
    }

    public String getTotal() {
        return total.get();
    }

    public StringProperty totalProperty() {
        return total;
    }

    public String getCurrent() {
        return current.get();
    }

    public StringProperty currentProperty() {
        return current;
    }

    public String getSpeed() {
        return speed.get();
    }

    public StringProperty speedProperty() {
        return speed;
    }

    public State getState() {
        return state.get();
    }

    public void setState(State state) {
        this.state.set(state);
    }

    public ObjectProperty<State> stateProperty() {
        return state;
    }

    public int getFiles() {
        return files.get();
    }

    public IntegerProperty filesProperty() {
        return files;
    }

    public double getPercent() {
        return percent.get();
    }

    public DoubleProperty percentProperty() {
        return percent;
    }

    public enum State {
        REQUESTING,
        PREPARING,
        DOWNLOADING,
        INSTALLING,
        SUCCESS,
        FAILED
    }

    class Request extends Task<Void> {

        final Server server;

        Request(Server server) {
            this.server = server;
        }

        @Override
        protected void failed() {
            super.failed();
            EventManager
                    .post(new LauncherErrorEvent("Ошибка при проверке обновлений", getException()));
        }

        @Override
        protected Void call() throws Exception {
            downloadManager.addDownload(updateManager.getUpdates(server));
            return null;
        }
    }

    class Install extends Task<Void> {

        final Server server;

        Install(Server server) {
            this.server = server;
        }

        @Override
        protected void failed() {
            super.failed();
            EventManager
                    .post(new LauncherErrorEvent("Ошибка при установке обновлений", getException()));
        }

        @Override
        protected Void call() throws Exception {
            updateManager.installUpdates(server);
            return null;
        }
    }
}

