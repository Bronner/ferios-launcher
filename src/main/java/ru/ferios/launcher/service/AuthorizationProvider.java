package ru.ferios.launcher.service;

import com.github.kevinsawicki.http.HttpRequest;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import lombok.extern.slf4j.Slf4j;
import ru.ferios.launcher.EventManager;
import ru.ferios.launcher.exception.LauncherException;
import ru.ferios.launcher.Properties;
import ru.ferios.launcher.exception.ResponceException;
import ru.ferios.launcher.ThreadManager;
import ru.ferios.launcher.domain.AuthorizationResponse;
import ru.ferios.launcher.domain.Credentials;
import ru.ferios.launcher.event.LauncherErrorEvent;
import ru.ferios.launcher.util.json.JsonHelper;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class AuthorizationProvider {

    private ObjectProperty<AuthorizationResponse> response = new SimpleObjectProperty<>();

    private ObjectProperty<State> state = new SimpleObjectProperty<>();

    public void authorize(Credentials credentials) {
        Request request = new Request(credentials);
        response.bind(request.valueProperty());
        request.setOnScheduled(event -> state.setValue(State.REQUESTING));
        request.setOnSucceeded(event -> state.setValue(State.SUCCEED));
        request.setOnFailed(event -> state.setValue(State.FAILED));
        ThreadManager.execute(request);
    }

    public ReadOnlyObjectProperty<AuthorizationResponse> responseProperty() {
        return response;
    }

    public ReadOnlyObjectProperty<State> stateProperty() {
        return state;
    }

    public enum State {
        REQUESTING,
        SUCCEED,
        FAILED
    }

    class Request extends Task<AuthorizationResponse> {

        private Credentials credentials;

        Request(Credentials credentials) {
            this.credentials = credentials;
        }

        @Override
        protected void failed() {
            super.failed();
            EventManager.post(new LauncherErrorEvent("Ошибка при авторизации", getException()));
        }

        @Override
        public AuthorizationResponse call() throws Exception {
            log.info("Авторизация...");

            Map<String, String> params = new HashMap<>();
            params.put("username", credentials.getLogin());
            params.put("password", credentials.getPassword());

            final HttpRequest request =
                    HttpRequest.post(Properties.API_AUTH).form(params).trustAllCerts();
            if (request.ok()) {
                log.info("Авторизация завершена");
                return JsonHelper.fromJson(request.reader(), AuthorizationResponse.class);
            } else if (request.badRequest()) {
                throw JsonHelper.fromJson(request.reader(), ResponceException.class);
            } else {
                throw new LauncherException(request.message());
            }
        }
    }
}

