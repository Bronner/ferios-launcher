package ru.ferios.launcher.service;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.reflect.TypeToken;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import ru.ferios.launcher.EventManager;
import ru.ferios.launcher.Properties;
import ru.ferios.launcher.domain.Server;
import ru.ferios.launcher.event.LauncherErrorEvent;
import ru.ferios.launcher.util.json.JsonHelper;

import java.util.ArrayList;

@Slf4j
public final class ServerListProvider extends Service<ObservableList<Server>> {

    @Override
    protected Task<ObservableList<Server>> createTask() {
        return new Task<ObservableList<Server>>() {
            //            @Override protected void scheduled() {
            ////                authController.setDisabled(true);
            //            }
            //
            @Override
            protected void succeeded() {
                log.info("Список серверов был получен");
            }

            //
            //            @Override protected void done() {
            //                super.done();        log.info("Список серверов был получен");
            //                Collection<Server> remoteServers = getValue();
            ////                ComboBox<Server> comboBox = authController.getServerField();
            ////                if (remoteServers.isEmpty()) {
            ////                    comboBox.setPromptText("Сервера отсутствуют");
            ////                } else {
            ////                    comboBox.setPromptText("Выберите сервер");
            ////                }
            ////                comboBox.getItems().clear();
            ////                comboBox.getItems().addAll(remoteServers);
            //
            //                //        try {
            //                //            comboBox.setValue(comboBox.getItems().get(Settings.getInstance().getServer()));
            //                //        } catch (IndexOutOfBoundsException e) {
            //                //            comboBox.setValue(comboBox.getItems().get(0));
            //                //        }
            //
            ////                authController.setDisabled(remoteServers.isEmpty());
            //            }
            //
            @Override
            protected void failed() {
                EventManager.post(
                        new LauncherErrorEvent("Ошибка при загрузке списка серверов", getException()));
                //                authController.getServerField().setPromptText("Сервера отсутствуют");
            }

            @Override
            protected ObservableList<Server> call() throws Exception {
                log.info("Получение списка серверов");
                HttpRequest request = HttpRequest.get(Properties.API_SERVERS).trustAllCerts();
                Validate.isTrue(request.ok(), request.message());
                return FXCollections.observableList(JsonHelper.fromJson(request.reader(),
                        new TypeToken<ArrayList<Server>>() {}.getType()));
            }
        };
    }
}
