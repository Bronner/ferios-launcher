package ru.ferios.launcher.domain;

import lombok.Getter;

import java.util.UUID;

@Getter
public class AuthorizationResponse {

    private UUID uuid;

    private String username;

    private String rememberToken;

    private String session;
}
