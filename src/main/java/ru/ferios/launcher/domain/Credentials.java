package ru.ferios.launcher.domain;

import lombok.Data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
public class Credentials {

    private static final Pattern LOGIN_PATTERN = Pattern.compile("^[\\w]{3,14}$");

    private static final Pattern PASSWORD_PATTERN = Pattern.compile("^.{3,}$");

    /**
     * Логин
     */
    private final String login;

    /**
     * Пароль пользователя
     */
    private final String password;

    /**
     * Сервер, на который заходит пользователь
     */
    private final Server server;

    /**
     * Сессия для авторизации
     */
    private String session;

    /**
     * Является ли пароль хешем или же был указан настоящий пароль
     */
    private boolean hashed = false;

    /**
     * Уникальный идентификатор пользователя
     */
    private String uuid;

    public boolean isValid() {
        Matcher login = LOGIN_PATTERN.matcher(this.login);
        Matcher password = PASSWORD_PATTERN.matcher(this.password);
        return login.matches() && password.matches();
    }
}
