package ru.ferios.launcher.domain;

import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExtractRules {

    @Getter
    private List<String> exclude = new ArrayList<>();

    public ExtractRules(String... exclude) {
        if (exclude != null) {
            Collections.addAll(this.exclude, exclude);
        }
    }

    public ExtractRules(@NonNull ExtractRules rules) {
        for (String exclude : rules.exclude) {
            this.exclude.add(exclude);
        }
    }

    public boolean shouldExtract(String path) {
        if (this.exclude != null) {
            for (String rule : this.exclude) {
                if (path.startsWith(rule)) {
                    return false;
                }
            }
        }
        return true;
    }
}
