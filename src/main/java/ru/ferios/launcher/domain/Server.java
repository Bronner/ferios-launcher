package ru.ferios.launcher.domain;

import com.google.gson.annotations.Expose;
import lombok.Data;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Data
public class Server {

    /**
     * Порядковый номер сервера в списке
     */
    private int id;

    /**
     * IP адрес сервера
     */
    private String ip;

    /**
     * Порт сервера
     */
    private int port;

    /**
     * Человекопонятное название сервера
     */
    private String name;

    /**
     * Уровень доступа сервера
     */
    private byte accessLevel;

    /**
     * Название клиента
     */
    private String clientName;

    private ResourceObject client;

    /**
     * Название набора библиотек
     */
    private String libListName;

    @Expose
    private Set<ResourceObject> libList = new HashSet<>();

    /**
     * Название набора модов
     */
    private String modListName;

    @Expose
    private Set<ResourceObject> modList = new HashSet<>();

    public String getAssets() {
        String clientVer = getClientVersion();
        return Integer.parseInt(clientVer.replaceAll("[\\W]", "")) > 172 ? clientVer : "legacy";
    }

    public String getClientVersion() {
        String local = clientName;
        if (clientName.contains("-")) {
            local = clientName.split("-")[0];
        }
        return local;
    }

    public Collection<ResourceObject> getLibList() {
        return Collections.unmodifiableCollection(libList);
    }

    public void setLibList(Collection<ResourceObject> list) {
        libList.clear();
        libList.addAll(list);
    }

    public Collection<ResourceObject> getModList() {
        return Collections.unmodifiableCollection(modList);
    }

    public void setModList(Collection<ResourceObject> list) {
        modList.clear();
        modList.addAll(list);
    }

    public String toString() {
        return name;
    }
}
