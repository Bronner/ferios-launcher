package ru.ferios.launcher.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode
@Getter
public class LauncherInfo {

    private String version;

    private String exeUrl;

    private String jarUrl;
}
