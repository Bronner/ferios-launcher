package ru.ferios.launcher.domain;

import ru.ferios.commons.utils.Platform;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Rule {

    private Action action = Action.ALLOW;

    private OSRestriction os;

    public Rule() {
    }

    public Rule(Rule rule) {
        this.action = rule.action;

        if (rule.os != null) {
            this.os = new OSRestriction(rule.os);
        }
    }

    public Action getAppliedAction() {
        if ((this.os != null) && (!this.os.isCurrentOperatingSystem())) {
            return null;
        }
        return this.action;
    }

    public String toString() {
        return "Rule{action=" + this.action + ", os=" + this.os + '}';
    }

    public static enum Action {
        ALLOW,
        DISALLOW
    }

    public class OSRestriction {

        private Platform platform;

        private String version;

        public OSRestriction(OSRestriction osRestriction) {
            this.platform = osRestriction.platform;
            this.version = osRestriction.version;
        }

        public boolean isCurrentOperatingSystem() {
            if ((this.platform != null) && (this.platform != Platform.CURRENT)) {
                return false;
            }
            if (this.version != null) {
                try {
                    Pattern pattern = Pattern.compile(this.version);
                    Matcher matcher = pattern.matcher(
                            System.getProperty("os.version")
                    );
                    if (!matcher.matches()) {
                        return false;
                    }
                } catch (Exception ignored) {
                    //NoP
                }
            }
            return true;
        }

        public String toString() {
            return "OSRestriction{platform=" + this.platform + ", version='" + this.version + '\'' + '}';
        }
    }
}
