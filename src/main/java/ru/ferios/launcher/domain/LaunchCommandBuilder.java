package ru.ferios.launcher.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.apache.commons.lang3.Validate.notNull;

public class LaunchCommandBuilder {

    private String username;

    private String version;

    private String gameDir;

    private String assetsDir;

    private String assetIndex;

    private String uuid;

    private String accessToken;

    private String userProperties = "{}";

    private String userType = "mojang";

    private Set<String> tweakClass = new HashSet<>();

    private String server;

    private String port;

    private boolean fullscreen;

    private String width;

    private String height;

    public LaunchCommandBuilder username(String username) {
        this.username = username;
        return this;
    }

    public LaunchCommandBuilder version(String version) {
        this.version = version;
        return this;
    }

    public LaunchCommandBuilder gameDir(String gameDir) {
        this.gameDir = gameDir;
        return this;
    }

    public LaunchCommandBuilder assetsDir(String assetsDir) {
        this.assetsDir = assetsDir;
        return this;
    }

    public LaunchCommandBuilder assetIndex(String assetIndex) {
        this.assetIndex = assetIndex;
        return this;
    }

    public LaunchCommandBuilder uuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public LaunchCommandBuilder accessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public LaunchCommandBuilder userProperties(String userProperties) {
        this.userProperties = userProperties;
        return this;
    }

    public LaunchCommandBuilder userType(String userType) {
        this.userType = userType;
        return this;
    }

    public LaunchCommandBuilder tweakClass(String tweakClass) {
        this.tweakClass.add(tweakClass);
        return this;
    }

    public boolean isTweak() {
        return !tweakClass.isEmpty();
    }

    public LaunchCommandBuilder server(String server, String port) {
        this.server = server;
        this.port = port;
        return this;
    }

    public LaunchCommandBuilder fullscreen() {
        this.fullscreen = true;
        return this;
    }

    public LaunchCommandBuilder resolution(String width, String height) {
        this.width = width;
        this.height = height;
        return this;
    }

    public String[] build() {
        List<String> list = new ArrayList<>();

        notNull(username);
        list.add("--username");
        list.add(username);

        notNull(version);
        list.add("--version");
        list.add(version);

        notNull(gameDir);
        list.add("--gameDir");
        list.add(gameDir);

        notNull(assetsDir);
        list.add("--assetsDir");
        list.add(assetsDir);

        notNull(assetIndex);
        list.add("--assetIndex");
        list.add(assetIndex);

        notNull(uuid);
        list.add("--uuid");
        list.add(uuid);

        notNull(accessToken);
        list.add("--accessToken");
        list.add(accessToken);

        notNull(userProperties);
        list.add("--userProperties");
        list.add(userProperties);

        notNull(userType);
        list.add("--userType");
        list.add(userType);

        for (String tweak : tweakClass) {
            list.add("--tweakClass");
            list.add(tweak);
        }

        if (server != null && port != null) {
            list.add("--server");
            list.add(server);
            list.add("--port");
            list.add(port);
        }

        if (fullscreen) {
            list.add("--fullscreen");
            list.add("true");
        } else if (width != null && height != null) {
            list.add("--width");
            list.add(width);
            list.add("--height");
            list.add(height);
        }

        return list.toArray(new String[list.size()]);
    }

}
