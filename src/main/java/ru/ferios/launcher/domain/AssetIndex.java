package ru.ferios.launcher.domain;

import lombok.Data;
import lombok.Getter;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class AssetIndex {

    public static final String DEFAULT_ASSET_NAME = "legacy";

    @Getter
    private final Map<String, AssetObject> objects;

    @Getter
    private boolean virtual;

    public AssetIndex() {
        this.objects = new LinkedHashMap<>();
    }

    public Collection<AssetObject> getUniqueObjects() {
        return objects.values();
    }

    @Data
    public class AssetObject {

        private String hash;

        private long size;

        private boolean reconstruct;

        private String compressedHash;

        private long compressedSize;

        private String filename;

        public String getFilename() {
            if (filename == null) {
                filename = getHash().substring(0, 2) + "/" + getHash();
            }
            return filename;
        }
    }
}
