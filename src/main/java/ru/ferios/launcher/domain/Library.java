package ru.ferios.launcher.domain;

import lombok.Data;
import org.apache.commons.lang3.text.StrSubstitutor;
import ru.ferios.commons.utils.Platform;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Library {

    private static final StrSubstitutor SUBSTITUTOR;

    static {
        HashMap<String, String> map = new HashMap<>();
        map.put("platform", Platform.CURRENT.getName());
        map.put("arch", Platform.Arch.CURRENT.asString());
        SUBSTITUTOR = new StrSubstitutor(map);
    }

    private String name;

    private List<Rule> rules;

    private Map<Platform, String> natives;

    private ExtractRules extract;

    private String url;

    public boolean appliesToCurrentEnvironment() {
        if (this.rules == null) {
            return true;
        }
        Rule.Action lastAction = Rule.Action.DISALLOW;

        for (Rule compatibilityRule : this.rules) {
            Rule.Action action = compatibilityRule.getAppliedAction();
            if (action != null) {
                lastAction = action;
            }
        }
        return lastAction == Rule.Action.ALLOW;
    }

    public String getArtifactPath() {
        return getArtifactPath(null);
    }

    public String getArtifactPath(String classifier) {
        if (this.name == null) {
            throw new IllegalStateException("Cannot get artifact path of empty/blank artifact");
        }
        return String.format("%s/%s", new Object[]{getArtifactBaseDir(), getArtifactFilename(classifier)});
    }

    public String getArtifactBaseDir() {
        if (this.name == null) {
            throw new IllegalStateException("Cannot get artifact dir of empty/blank artifact");
        }
        String[] parts = this.name.split(":", 3);
        return String.format("%s/%s/%s", new Object[]{parts[0].replaceAll("\\.", "/"), parts[1], parts[2]});
    }

    public String getArtifactFilename(String classifier) {
        if (this.name == null) {
            throw new IllegalStateException("Cannot get artifact filename of empty/blank artifact");
        }
        String[] parts = this.name.split(":", 3);
        String result = String.format("%s-%s%s.jar", new Object[]{parts[1], parts[2], "-" + classifier});

        return SUBSTITUTOR.replace(result);
    }
}
