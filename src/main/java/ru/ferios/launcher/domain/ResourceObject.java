package ru.ferios.launcher.domain;

import lombok.Data;

@Data
public class ResourceObject {

    private String path;

    private String name;

    private String hash;

    private Long size;
}
